package me.deft.expanse.objects.ship;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by iyakovlev on 06.07.17.
 */

public interface Gun {

    void setAngle(float angle);

    float getAngle();

    Bullet shoot(float dt);

    void setPosition(Vector2 position);

    void setFireRate(float seconds);

    void setDamage(int damage);

}
