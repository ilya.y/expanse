package me.deft.expanse.objects;

/**
 * Created by iyakovlev on 21.07.17.
 */

public interface ObservableGameObject {

    interface Observer {
        void onDestroy();
    }

    void setObserver(Observer observer);
    void removeObserver();
}
