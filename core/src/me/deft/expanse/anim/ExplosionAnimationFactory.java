package me.deft.expanse.anim;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import me.deft.expanse.Global;
import me.deft.expanse.objects.utils.AbstractFactory;

/**
 * Created by iyakovlev on 24.07.17.
 */

public class ExplosionAnimationFactory extends AbstractFactory<Explosion> {

    public ExplosionAnimationFactory(SpriteBatch spriteBatch, TextureAtlas textureAtlas) {
        super(spriteBatch, textureAtlas);
    }

    @Override
    public Explosion get() {
        return new Explosion(mSpriteBatch, 0.02f, Global.getInstance().getExplosionTexture(), 8);
    }
}
