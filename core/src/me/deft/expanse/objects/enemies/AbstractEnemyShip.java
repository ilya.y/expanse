package me.deft.expanse.objects.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.objects.AbstractCollidableGameObject;
import me.deft.expanse.objects.AbstractGameObject;
import me.deft.expanse.objects.CollidableGameObject;
import me.deft.expanse.objects.EventProducer;
import me.deft.expanse.objects.GameObject;
import me.deft.expanse.objects.ship.Bullet;
import me.deft.expanse.objects.ship.Gun;
import me.deft.expanse.objects.ship.Ship;
import me.deft.expanse.objects.utils.MathUtils;
import me.deft.expanse.objects.utils.Route;

/**
 * Created by iyakovlev on 09.07.17.
 */

public abstract class AbstractEnemyShip extends AbstractCollidableGameObject {
    private Route mRoute;
    private Gun mGun;
    private float mSpeed;
    private Vector2 mVectorSpeed = new Vector2();
    private GameObject mTarget;

    private Vector2 mCurrentDestination;
    private boolean mIsDone = false;

    protected Ship.FireListener mFireListener;

    public AbstractEnemyShip(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture);
    }

    public void setRoute(Route route) {
        mRoute = route;
        setPosition(mRoute.getNextPoint());
        getPolygon().setPosition(getPosition().x, getPosition().y);
        mCurrentDestination = mRoute.getNextPoint();
    }

    public void setFireListener(Ship.FireListener fireListener) {
        mFireListener = fireListener;
    }

    public void setGun(Gun gun) {
        mGun = gun;
    }

    public void setSpeed(float speed) {
        mSpeed = speed;
        mVectorSpeed.set(speed, speed);
    }

    @Override
    public void update(float timeDelta) {
        if (isDone())
            return;
        if (Math.round(getPosition().x) == mCurrentDestination.x && Math.round(getPosition().y) == mCurrentDestination.y) {;
            if (!mRoute.isFinished()) {
                mCurrentDestination = mRoute.getNextPoint();
                update(timeDelta);
            } else {
                mIsDone = true;
            }
        }

        mGun.setAngle(MathUtils.getAngle(getPosition(), mTarget.getPosition()));
        mGun.setPosition(getPosition());
             Vector2 n = mCurrentDestination.cpy().sub(getPosition()).nor();
        setPosition(getPosition().mulAdd(n, mSpeed * timeDelta));

        getPolygon().setPosition(getPosition().x, getPosition().y);
        shoot(timeDelta);
    }

    @Override
    public boolean isNeedToBeDestroyed() {
        return mIsDone || mHealth <= 0;
    }

    public boolean isDone() {
        return mIsDone;
    }

    @Override
    protected void onZeroHealth() {
        mIsDone = true;
    }

    public void setTarget(GameObject object) {
        mTarget = object;
    }

    @Override
    public void takeDamage(int dmg) {
        super.takeDamage(dmg);
    }

    @Override
    public Vector2 getVelocity() {
        return mVectorSpeed;
    }

    @Override
    public int getDamage() {
        return 1;
    }

    @Override
    public boolean collide(CollidableGameObject otherObject) {
        Polygon p = new Polygon();
        if (otherObject instanceof Bullet) {
            if (Intersector.intersectPolygons(getPolygon(), otherObject.getPolygon(),  p)) {
                takeDamage(otherObject.getDamage());
                otherObject.takeDamage(getDamage());
            }
            return true;
        }
        return false;
    }



    public void shoot(float dt) {
        if (mIsDone)
            return ;

        if (mGun == null)
            return;

        Bullet b = mGun.shoot(dt);

        if (b != null) {
            if (mFireListener != null)
                mFireListener.onFire(b);
        }
    }
}
