package me.deft.expanse.objects.utils;

/**
 * Created by iyakovlev on 09.07.17.
 */

public class RoutesFactory implements ObjectPool.ObjectFactory<Route> {
    @Override
    public Route get() {
        return new Route();
    }
}