package me.deft.expanse;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import me.deft.expanse.screens.ScreenType;
import me.deft.expanse.screens.ScreensManager;

/**
 * Created by iyakovlev on 17.07.17.
 */

public class ExpanseGame2 extends com.badlogic.gdx.Game {

    public static final int DEFAULT_WIDTH = 1280;
    public static final int DEFAULT_HEIGHT = 720;

    SpriteBatch mSpriteBatch;
    Camera mCamera;
    Viewport mViewport;

    public static boolean IS_ANDROID = false;
    public static boolean IS_DEBUG = false;
    private static ExpanseGame2 ourInstance;
    public static ExpanseGame2 getInstance() {
        if (ourInstance == null)
            throw new IllegalArgumentException("wtf?");
        return ourInstance;
    }

    public ExpanseGame2() {
        ourInstance = this;
    }

    /**
     * Сделал большой рефакторинг сцены, стало немного аккуратнее.
     * Сделал меню, правда тяп-ляп пока, ну хоть что-то.
     * Камеру сделал в классе ExpanseGame2, т.к. это, по-идее, сам по себе синглтон,
     * да и SpriteBatch я тут создаю
     * Добавил звуки разрушения астероида, столкновения корабля с чем-то (кроме пуль)
     * Добавил музыку в главное меню
     *
     *  TODO:
     *      1. Сделать процессор коллизий для Collidable объектов, вынести обсчёт столкновений из
     *      класса объекта в этот процессор.
     *      2. Сделать для объектов Z-index, т.к. сейчас они рисуются в произвольном порядке - могут
     *      хаотично перекрываться
     *   +  3. Переделать создание противников на PeriodicEventsManager, сейчас суперикриво пока
     *      4. Сделать усиления временными, сейчас QuadDamage действует бесконечно
     *      5. Настройки музыки
     */

    public static void setDebug(boolean isDebug) {
        IS_DEBUG = isDebug;
    }


    @Override
    public void create() {
        mSpriteBatch = new SpriteBatch();
        mCamera = new OrthographicCamera(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        mViewport = new FitViewport(DEFAULT_WIDTH, DEFAULT_HEIGHT, mCamera);
        mViewport.update(DEFAULT_WIDTH, DEFAULT_HEIGHT, true);
        mViewport.apply();
        mSpriteBatch.setProjectionMatrix(mCamera.combined);

        ScreensManager.getInstance().init(this, mSpriteBatch).setScreen(ScreenType.MENU);
    }

    public Viewport getViewport() {
        return mViewport;
    }

    @Override
    public void dispose() {
        super.dispose();
        mSpriteBatch.dispose();
    }

    @Override
    public void render() {
        float dt = Gdx.graphics.getDeltaTime();
        getScreen().render(dt);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        mViewport.update(width, height, true);
        mViewport.apply();
    }
}
