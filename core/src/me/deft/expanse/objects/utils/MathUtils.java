package me.deft.expanse.objects.utils;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by iyakovlev on 10.07.17.
 */

public class MathUtils {

    public static float getAngle(Vector2 from, Vector2 to) {
        float angle = (float) Math.toDegrees(Math.atan2(to.y - from.y, to.x - from.x));
        if (angle < 0){
            angle += 360;
        }

        return angle;
    }
}
