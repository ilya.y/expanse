package me.deft.expanse.objects.utils;

import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.ExpanseGame2;

/**
 * Created by iyakovlev on 20.07.17.
 */

public class CoordinatesManager {

    static Vector2 unproject(Vector2 coordinates) {
        return ExpanseGame2.getInstance().getViewport().unproject(coordinates);
    }
}
