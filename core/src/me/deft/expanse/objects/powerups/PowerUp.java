package me.deft.expanse.objects.powerups;

import me.deft.expanse.objects.CollidableGameObject;
import me.deft.expanse.objects.ship.Ship;

/**
 * Created by iyakovlev on 13.07.17.
 */

public interface PowerUp extends CollidableGameObject{

     void apply(Ship ship);

}
