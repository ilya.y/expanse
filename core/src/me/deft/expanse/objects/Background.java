package me.deft.expanse.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

import me.deft.expanse.ExpanseGame2;
import me.deft.expanse.Global;


/**
 * Created by iyakovlev on 05.07.17.
 */

public class Background implements GameObject {

    private SpriteBatch mSpriteBatch;
    private TextureRegion mStarTextureRegion;
    private Star[] mStars;
    private Vector2 mPosition = new Vector2(0,0);
    public Background(SpriteBatch spriteBatch) {
        mSpriteBatch = spriteBatch;
        mStarTextureRegion = Global.getInstance().getCurrentTextureAtlas().findRegion("star");
        mStars = new Star[100];

        Random r = new Random(System.nanoTime());
        for (int i = 0; i < mStars.length; i++) {
            mStars[i] = new Star(spriteBatch, mStarTextureRegion, r.nextInt(4));
        }

    }

    @Override
    public void draw() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        for (Star s : mStars)
            s.draw();
    }

    @Override
    public void update(float timeDelta) {
        for (Star s : mStars)
            s.update(timeDelta);
    }

    @Override
    public float getWidth() {
        return ExpanseGame2.DEFAULT_WIDTH;
    }

    @Override
    public float getHeight() {
        return ExpanseGame2.DEFAULT_HEIGHT;
    }

    @Override
    public Vector2 getPosition() {
        return mPosition;
    }

    @Override
    public void setPosition(float x, float y) {

    }

    private class Star extends AbstractGameObject {

        int mSize = 1;
        private final Vector2 mSpeed = new Vector2(-15,0);


        public Star(SpriteBatch spriteBatch, TextureRegion texture, int size) {
            super(spriteBatch, texture);
            mSize = size;
            if (mSize < 1)
                mSize = 1;
            if (mSize > 3)
                mSize = 3;

            int x = (int) (Math.random() * ExpanseGame2.DEFAULT_WIDTH);
            int y = (int) (Math.random() * ExpanseGame2.DEFAULT_HEIGHT);
            setPosition(x, y);
        }

        @Override
        public void draw() {
            mSpriteBatch.draw(
                    mTextureRegion,
                    getPosition().x,
                    getPosition().y,
                    0,
                    0,
                    mSize,
                    mSize,
                    1f + 1f / mSize,
                    1f + 1f /mSize,
                    0);
        }

        @Override
        public void update(float timeDelta) {
            setPosition(getPosition().x + mSpeed.x * timeDelta * mSize, getPosition().y + mSpeed.y * timeDelta * mSize);

            if (getPosition().x < 0 ) {
                getPosition().x = ExpanseGame2.DEFAULT_WIDTH;
                getPosition().y = (int) (Math.random() * ExpanseGame2.DEFAULT_HEIGHT);
            }

        }



    }
}
