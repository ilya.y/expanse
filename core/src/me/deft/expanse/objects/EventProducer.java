package me.deft.expanse.objects;

/**
 * Created by iyakovlev on 20.07.17.
 */

public interface EventProducer {

    interface EventListener {
        void onEventOccured();
    }

    boolean checkEvent(float dt);
}
