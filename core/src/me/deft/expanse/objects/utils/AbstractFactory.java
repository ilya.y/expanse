package me.deft.expanse.objects.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by iyakovlev on 05.07.17.
 */

public abstract class AbstractFactory<T> implements ObjectPool.ObjectFactory<T> {

    protected SpriteBatch mSpriteBatch;

    public AbstractFactory(SpriteBatch spriteBatch, TextureAtlas textureAtlas) {
        mSpriteBatch = spriteBatch;
    }
}
