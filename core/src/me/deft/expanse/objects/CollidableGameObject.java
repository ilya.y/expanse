package me.deft.expanse.objects;

import com.badlogic.gdx.math.*;

import java.net.CookieHandler;

/**
 * Created by iyakovlev on 03.07.17.
 * контракт для объектов, которые могут сталкиваться с другими объектами
 */
public interface CollidableGameObject extends GameObject {

    Polygon getPolygon();

    Vector2 getVelocity();

    int getDamage();

    void takeDamage(int dmg);

    float getWeight();

    int getHealth(); //наверное, это лишнее тут

    void setDamage(int damage);

    // returns true if collision was processed
    boolean collide(CollidableGameObject otherObject); //

    boolean isNeedToBeDestroyed();

}
