package me.deft.expanse.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.Global;
import me.deft.expanse.objects.GameObject;
import me.deft.expanse.objects.utils.GameInputProcessor;
import me.deft.expanse.screens.MenuScreen;

public class Button implements GameObject {

    public interface OnClickListener {
        void onClick();
    }

    private SpriteBatch mSpriteBatch;
    private TextureRegion mTextureRegion;
    private Vector2 mPosition;

    private Button.OnClickListener mOnClickListener;
    GameInputProcessor mGameInputProcessor;

    public Button(SpriteBatch spriteBatch, TextureRegion textureRegion) {
        mSpriteBatch = spriteBatch;
        mTextureRegion = textureRegion;
        mGameInputProcessor = Global.getInstance().getGameInputProcessor();
        mPosition = new Vector2(0,0);
    }

    public void setOnClickListener(Button.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public void draw() {
        mSpriteBatch.draw(mTextureRegion, mPosition.x, mPosition.y);
    }

    @Override
    public void update(float timeDelta) {
        if (mGameInputProcessor.checkTouchRegion(
                (int)mPosition.x,
                (int)mPosition.y,
                mTextureRegion.getRegionWidth(),
                mTextureRegion.getRegionHeight())) {
            if (mOnClickListener != null) {
                mOnClickListener.onClick();
            }
        }
    }

    @Override
    public float getWidth() {
        return mTextureRegion.getRegionWidth();
    }

    @Override
    public float getHeight() {
        return mTextureRegion.getRegionHeight();
    }

    @Override
    public Vector2 getPosition() {
        return mPosition;
    }

    @Override
    public void setPosition(float x, float y) {
        mPosition.x = x;
        mPosition.y = y;
    }
}