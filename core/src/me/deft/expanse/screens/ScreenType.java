package me.deft.expanse.screens;

/**
 * Created by iyakovlev on 17.07.17.
 */

public enum ScreenType {
    MENU,
    LEVEL_1,
    LEVEL_2
}
