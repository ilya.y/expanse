package me.deft.expanse.objects.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

import me.deft.expanse.ExpanseGame2;
import me.deft.expanse.Global;
import me.deft.expanse.objects.enemies.*;
import me.deft.expanse.objects.powerups.ExtraHealth;
import me.deft.expanse.objects.powerups.PowerUp;
import me.deft.expanse.objects.powerups.QuadDamagePowerUp;
import me.deft.expanse.objects.ship.Gun;
import me.deft.expanse.objects.ship.MachineGun;
import me.deft.expanse.objects.ship.Rocinante;
import me.deft.expanse.objects.ship.Ship;

/**
 * Created by iyakovlev on 05.07.17.
 */

public class GameObjectsFactory {
    SpriteBatch mSpriteBatch;

    ObjectPool<Asteroid> mAsteroidObjectPool;
    ObjectPool<Route> mRouteObjectPool;

    public GameObjectsFactory(SpriteBatch spriteBatch) {
        this.mSpriteBatch = spriteBatch;


        mAsteroidObjectPool = new ObjectPool<>(new me.deft.expanse.objects.enemies.AsteroidFactory(spriteBatch, Global.getInstance().getCurrentTextureAtlas()));
        mRouteObjectPool = new ObjectPool<>(new RoutesFactory());
    }

    public Asteroid getRandomAsteroid(Vector2 position) {
        Random r = new Random(System.nanoTime());
        int x = -(r.nextInt(150) + 55);
        int y = r.nextInt(15) ;

        if (position.y > ExpanseGame2.DEFAULT_HEIGHT / 2) {
            y = -y;
        }

        Vector2 randomSpeed = new Vector2((float)x, (float)y);
        Asteroid asteroid = mAsteroidObjectPool.get();
        asteroid.init(randomSpeed, position.x, position.y, r.nextFloat() + 0.2f, 100);
        return asteroid;
    }


    public Ship getShip() {
        return new Rocinante(this.mSpriteBatch, Global.getInstance().getCurrentTextureAtlas().findRegion("roci"));
    }

    public Route getRoute() {
        return mRouteObjectPool.get();
    }

    public AbstractEnemyShip getTestEnemyShip() {
        Route r = getRoute();
        r.init(new float[]{300, ExpanseGame2.DEFAULT_HEIGHT, 100, 100, 300, 300, 100, 400, 400, 400, 500, 500, 600, 300, 300, 600, 300, -300});
        RegularEnemyShip sh = new RegularEnemyShip(mSpriteBatch, Global.getInstance().getCurrentTextureAtlas().findRegion("enemy"));
        sh.setGun(getGun(0.5f));
        sh.setRoute(r);
        sh.setSpeed(100);
        return sh;
    }

    public Gun getGun(float rate) {
        Gun gun = new MachineGun(mSpriteBatch);
        gun.setFireRate(rate);
        return gun;
    }

    public Squad getRandomSquad() {
        Route r = getRoute();
        r.init(new float[] {800, ExpanseGame2.DEFAULT_HEIGHT, 800, -200});
        Squad squad = new Squad(r, 1.5f);
        int num = (int) (Math.random() * 10) % 4 + 1;
        for (int i = 0; i < num; i++) {
            squad.addShip(getTestEnemyShip());
            squad.addShip(getTestEnemyShip());
            squad.addShip(getTestEnemyShip());
        }

        return squad;
    }

    public PowerUp getRandomPowerUp() {
        PowerUp pwrp;
        if (Math.random() < 0.5) {
            pwrp = new QuadDamagePowerUp(mSpriteBatch, Global.getInstance().getCurrentTextureAtlas().findRegion("bolt_gold"));
        } else {
            pwrp = new ExtraHealth(mSpriteBatch, Global.getInstance().getCurrentTextureAtlas().findRegion("shield_gold"));
        }
        pwrp.setPosition(ExpanseGame2.DEFAULT_WIDTH, (float) ((ExpanseGame2.DEFAULT_HEIGHT - 50) * Math.random()));
        return pwrp;
    }
}