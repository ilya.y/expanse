package me.deft.expanse.objects.utils;

import java.util.LinkedList;

/**
 * Created by iyakovlev on 05.07.17.
 */
public class ObjectPool <T extends ObjectPool.PoolItem>{

    public interface PoolItem<T extends PoolItem> {

        void dispose();

        void setPool(ObjectPool<T> pool);

    }

    public interface ObjectFactory<T> {
        T get();
    }

    ObjectFactory<T> mObjectFactory;
    LinkedList<T> mAvailableObjects;

    public ObjectPool(ObjectFactory<T> factory) {
        mAvailableObjects = new LinkedList<>();
        mObjectFactory = factory;
    }

    public void recycle(T item) {
        mAvailableObjects.add(item);
    }

    public T get() {
        T res = mAvailableObjects.poll();

        if (res == null) {
            res = mObjectFactory.get();
        }

        res.setPool(this);

        return res;
    }




}
