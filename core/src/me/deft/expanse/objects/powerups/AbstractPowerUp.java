package me.deft.expanse.objects.powerups;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.objects.AbstractCollidableGameObject;
import me.deft.expanse.objects.CollidableGameObject;

/**
 * Created by iyakovlev on 13.07.17.
 */

public abstract class AbstractPowerUp extends AbstractCollidableGameObject implements PowerUp {

    protected boolean mIsUsed;

    public AbstractPowerUp(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture);
        mVelocity = new Vector2(-25, 0);
    }

    @Override
    public void draw() {
        mSpriteBatch.draw(mTextureRegion, getPosition().x, getPosition().y);
    }



    @Override
    public void update(float timeDelta) {
        getPosition().set(getPosition().x + mVelocity.x * timeDelta, getPosition().y + mVelocity.y * timeDelta);
        super.update(timeDelta);
    }

    @Override
    public final int getDamage() {
        return 0;
    }

    @Override
    protected void onZeroHealth() {

    }

    @Override
    public boolean isNeedToBeDestroyed() {
        return mIsUsed;
    }

    @Override
    public final void setDamage(int damage) {

    }

    @Override
    public boolean collide(CollidableGameObject otherObject) {
        return false;
    }
}
