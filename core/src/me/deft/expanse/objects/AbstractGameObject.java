package me.deft.expanse.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by iyakovlev on 29.06.17.
 */
public abstract class AbstractGameObject implements GameObject {

    public SpriteBatch mSpriteBatch;
    public TextureRegion mTextureRegion;

    private int mWidth;
    private int mHeight;

    protected boolean mIsVisible;

    private Vector2 mPosition;


    public AbstractGameObject(SpriteBatch spriteBatch, TextureRegion texture) {
        mSpriteBatch = spriteBatch;
        mTextureRegion = texture;
        mWidth = mTextureRegion.getRegionWidth();
        mHeight = mTextureRegion.getRegionHeight();
        mIsVisible = true;
        mPosition = new Vector2(0,0);
    }

    @Override
    public void setPosition(float x, float y) {
        mPosition.x = x;
        mPosition.y = y;
    }

    public void setPosition(Vector2 vector) {
        setPosition(vector.x, vector.y);
    }

    @Override
    public Vector2 getPosition() {
        return mPosition;
    }

    @Override
    public float getWidth() {
        return mWidth;
    }

    @Override
    public float getHeight() {
        return mHeight;
    }
}
