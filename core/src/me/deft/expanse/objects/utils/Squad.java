package me.deft.expanse.objects.utils;

import com.badlogic.gdx.utils.Array;

import me.deft.expanse.objects.GameObject;
import me.deft.expanse.objects.enemies.AbstractEnemyShip;

/**
 * Created by iyakovlev on 10.07.17.
 * Класс, описывающий поведение групп противников - маршрут, интервал появления
 */

public class Squad extends LimitedPeriodicEventsManager {

    private Route mRoute;
    private Array<AbstractEnemyShip> mShips = new Array<>();
    private float mSpawnInterval;
    private float mCurrentSwawnInterval = 0;

    private boolean mIsDone = false;

    private OnShipReadyListener mOnShipReadyListener;

    public interface OnShipReadyListener {
        void onShipReady(AbstractEnemyShip ship);
    }

    public Squad (Route route, float timeInterval) {
        super(1f, timeInterval, 0);
        mRoute = route;
    }

    public void setOnShipReadyListener(OnShipReadyListener onShipReadyListener) {
        mOnShipReadyListener = onShipReadyListener;
    }

    public void addShip(AbstractEnemyShip ship) {
        ship.setRoute(mRoute.cpy());
        mShips.add(ship);
        mNumberOfEvents++;
    }

    public boolean isDone() {
        return mNumberOfEvents == 0;
    }


    @Override
    public boolean checkEvent(float dt) {
        if (super.checkEvent(dt)) {
            if (mOnShipReadyListener!= null)
                mOnShipReadyListener.onShipReady(mShips.pop());

            return true;
        }
        return false;
    }


    public void setTarget(GameObject target) {
        for (AbstractEnemyShip ship : mShips)
            ship.setTarget(target);
    }

}
