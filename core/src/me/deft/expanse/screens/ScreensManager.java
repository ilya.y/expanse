package me.deft.expanse.screens;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import me.deft.expanse.Global;

/**
 * Created by iyakovlev on 17.07.17.
 */

public class ScreensManager {

    private static ScreensManager ourInstance;
    private Game mGame;
    private SpriteBatch mSpriteBatch;

//    HashMap<ScreenType, Screen> mScreensCache = new HashMap<>();

    public static ScreensManager getInstance() {
        if (ourInstance == null)
            ourInstance = new ScreensManager();
        return ourInstance;
    }

    public ScreensManager init(Game game, SpriteBatch spriteBatch) {
        mGame = game;
        mSpriteBatch = spriteBatch;
        return this;
    }

    public void setScreen(ScreenType type) {
        if (mGame == null)
            throw new IllegalStateException("Game should be set");
        Screen scr = mGame.getScreen();
        if (scr!= null)
            scr.dispose();

        Global.getInstance().clearAssets();
        Global.getInstance().loadAssets(type);

        mGame.setScreen(getScreenInstance(type));
    }

    private Screen getScreenInstance(ScreenType type) {
       // return new Level1Screen(mSpriteBatch);
        Global.getInstance().loadAssets(type);
        switch (type) {
            case MENU:
                return new MenuScreen(mSpriteBatch);
            case LEVEL_1:
                return new Level1Screen(mSpriteBatch);
        }

        return null;
    }
}
