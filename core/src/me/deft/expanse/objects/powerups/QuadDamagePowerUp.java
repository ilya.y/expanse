package me.deft.expanse.objects.powerups;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;

import me.deft.expanse.objects.ship.Ship;

/**
 * Created by iyakovlev on 13.07.17.
 */

public class QuadDamagePowerUp extends AbstractPowerUp {

    private Polygon mPolygon = new Polygon(new float[] {
            0, 0,
            0, 30,
            30, 30,
            30, 0
    });

    public QuadDamagePowerUp(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture);
    }

    @Override
    public void apply(Ship ship) {
        ship.setDamageMultiplier(4);
        mIsUsed = true;
    }

    @Override
    public Polygon getPolygon() {
        return mPolygon;
    }
}
