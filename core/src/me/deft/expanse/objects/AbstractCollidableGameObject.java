package me.deft.expanse.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by iyakovlev on 12.07.17.
 */

public abstract class AbstractCollidableGameObject extends AbstractGameObject implements CollidableGameObject {

    protected int mHealth;
    protected int mDamage;
    protected float mWeight;

    protected Vector2 mVelocity;

    public AbstractCollidableGameObject(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture);
        mVelocity = new Vector2(0,0);
        mDamage = 0;
        mHealth = 25;
    }

    protected abstract void onZeroHealth();

    @Override
    public void setDamage(int damage) {
        mDamage = damage;
    }

    @Override
    public Vector2 getVelocity() {
        return mVelocity;
    }

    @Override
    public void takeDamage(int dmg) {
        mHealth -= dmg;
        if (mHealth <= 0)
            onZeroHealth();
    }

    @Override
    public void update(float timeDelta) {
        getPolygon().setPosition(getPosition().x, getPosition().y);
    }

    @Override
    public float getWeight() {
        return 0;
    }

    @Override
    public int getHealth() {
        return mHealth;
    }
}
