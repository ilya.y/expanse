package me.deft.expanse;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import me.deft.expanse.objects.utils.GameInputProcessor;
import me.deft.expanse.screens.ScreenType;

/**
 * Created by iyakovlev on 09.07.17.
 */

public class Global {

    AssetManager mAssetManager;
    TextureAtlas mTextureAtlas;
    GameInputProcessor mGameInputProcessor;
    private static Global ourInstance;
    public static Global getInstance() {
        if (ourInstance == null)
            ourInstance = new Global();
        return ourInstance;
    }

    public AssetManager getAssetManager() {
        return mAssetManager;
    }

    private Global() {
        mAssetManager = new AssetManager();
        mAssetManager.load("sprites.atlas", TextureAtlas.class);
        mAssetManager.load("font.fnt", BitmapFont.class);
        mAssetManager.finishLoading();
        mGameInputProcessor = new GameInputProcessor();
        Gdx.input.setInputProcessor(mGameInputProcessor);
    }

    public GameInputProcessor getGameInputProcessor() {
        return mGameInputProcessor;
    }

    public void clearAssets() {
        mAssetManager.clear();
        mAssetManager.dispose();
        mAssetManager = new AssetManager();
    }

    public TextureAtlas getCurrentTextureAtlas() {
        return mTextureAtlas;
    }

    public Texture getExplosionTexture() {
        return mAssetManager.get("explosion.png", Texture.class);
    }

    public BitmapFont getFont() {
        return mAssetManager.get("font.fnt", BitmapFont.class);
    }

    public void loadAssets(ScreenType type) {
        switch (type) {
            case MENU:
                mAssetManager.load("menu.atlas", TextureAtlas.class);
                mAssetManager.load("font.fnt", BitmapFont.class);
                mAssetManager.load("theme.mp3", Music.class);
                mAssetManager.finishLoading();
                mTextureAtlas = mAssetManager.get("menu.atlas", TextureAtlas.class);
                break;

            case LEVEL_1:
                mAssetManager.load("sprites.atlas", TextureAtlas.class);
                mAssetManager.load("font.fnt", BitmapFont.class);
                mAssetManager.load("clank.wav", Sound.class);
                mAssetManager.load("rock.mp3", Sound.class);
                mAssetManager.load("explosion.png", Texture.class);
                mAssetManager.finishLoading();
                mTextureAtlas = mAssetManager.get("sprites.atlas", TextureAtlas.class);
                break;
        }
    }
}
