package me.deft.expanse.objects.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;

/**
 * Created by iyakovlev on 09.07.17.
 */

public class RegularEnemyShip extends AbstractEnemyShip {

    private final Polygon mPolygon = new Polygon(new float[]{
            0, 20,
            30, 20,
            62, 48,
            106, 48,
            120, 64,
            172, 48,
            172, 28,
            120, 4,
            0, 4
    });
    public RegularEnemyShip(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture);
    }

    @Override
    public Polygon getPolygon() {
        return mPolygon;
    }

    @Override
    public void draw() {
        mSpriteBatch.draw(
                mTextureRegion,
                getPosition().x,
                getPosition().y,
                0,
                0,
                mTextureRegion.getRegionWidth(),
                mTextureRegion.getRegionHeight(),
                1,
                1,
                0
        );
    }

    @Override
    public float getWeight() {
        return 1;
    }


}
