package me.deft.expanse.objects.ship;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import me.deft.expanse.Global;
import me.deft.expanse.anim.Explosion;
import me.deft.expanse.anim.ExplosionAnimationFactory;
import me.deft.expanse.objects.AbstractCollidableGameObject;
import me.deft.expanse.objects.AbstractGameObject;
import me.deft.expanse.objects.CollidableGameObject;
import me.deft.expanse.objects.enemies.AbstractEnemyShip;
import me.deft.expanse.objects.enemies.Asteroid;
import me.deft.expanse.objects.powerups.PowerUp;
import me.deft.expanse.objects.utils.MathUtils;
import me.deft.expanse.objects.utils.ObjectPool;

/**
 * Created by iyakovlev on 03.07.17.
 */
public abstract class AbstractShip extends AbstractCollidableGameObject implements Ship {

    List<ShipObserver> mShipObserverList = new LinkedList<>();

    protected FlickeringManager mFlickeringManager = new FlickeringManager();

    private Vector2 mShift;
    protected int mDamageMultiplier = 1;
    protected float mRotationSpeed = 45f;
    protected float mCurrentAngle = 0;
    private boolean mIsInvincible = false;
    private float mInvincibilityTimer = 0;

    protected FireListener mFireListener;

    private Sound mCollideSound;


    //я просто хотел по-быстрому сделать анимацию и посмотреть, как оно выглядт
    //поэтому просто по-быстромуд добави сюда и пул, и массив анимаций
    private Array<Explosion> mExplosions;
    private ObjectPool<Explosion> mExplosionObjectPool;

    public AbstractShip(SpriteBatch spriteBatch, TextureRegion texture, Vector2 speed) {
        super(spriteBatch, texture);
        this.setPosition(100, 200);
        mShift = new Vector2(0,0);
        mVelocity = speed;
        mHealth = 25;
        mDamage = 1;

        mCollideSound = Global.getInstance().getAssetManager().get("clank.wav", Sound.class);

        mExplosions = new Array<>();
        mExplosionObjectPool = new ObjectPool<>(new ExplosionAnimationFactory(mSpriteBatch, null));
    }

    public void startFlicker(float numberOfTimes, float time) {
        if (!mFlickeringManager.isFlickering)
            mFlickeringManager.startFlicker(numberOfTimes, time);
    }

    @Override
    public void addOnFireListener(FireListener fireListener) {
        mFireListener = fireListener;
    }

    @Override
    protected void onZeroHealth() {
        setInvincible(3);
    }

    @Override
    public void addObserver(ShipObserver observer) {
        mShipObserverList.add(observer);
    }

    @Override
    public void removeObserver(ShipObserver observer) {
        mShipObserverList.remove(observer);
    }

    private void notifyHealthChanged() {
        for (ShipObserver observer : mShipObserverList)
            observer.onHealthChanged(mHealth);
    }

    @Override
    public void update(float timeDelta) {
        super.update(timeDelta);
        if (mFlickeringManager.isFlickering) {
            mIsVisible = mFlickeringManager.isVisible(timeDelta);
        } else {
            mIsVisible = true;
        }

        handleInput(timeDelta);
       // getPosition().add(mShift.scl(timeDelta));
        checkInvincible(timeDelta);
        Polygon p = getPolygon();
        p.setOrigin(getOriginX(), getOriginY());
        p.setRotation(getCurrentAngle());
        updateExplosions(timeDelta);
       // mShift.setZero();
    }

    protected void updateExplosions(float timeDelta) {
        Iterator<Explosion> iter = mExplosions.iterator();
        while (iter.hasNext()) {
            Explosion e = iter.next();
            if (e.isFinished()) {
                e.dispose();
                iter.remove();
                continue;
            }
            e.update(timeDelta);
        }
    }

    @Override
    public boolean isNeedToBeDestroyed() {
        return false;
    }

    @Override
    public void setInvincible(float time) {
        mIsInvincible = true;
        mInvincibilityTimer = time;
        startFlicker(time, time);
    }

    @Override
    public void takeDamage(int dmg) {
        if (!isInvincible()) {
            super.takeDamage(dmg);
            notifyHealthChanged();
        }
    }

    @Override
    public boolean isInvincible() {
        return mIsInvincible;
    }

    public void checkInvincible(float dt) {
        mInvincibilityTimer -= dt;

        if (mInvincibilityTimer < 0)
            mIsInvincible = false;
    }

    @Override
    public void rotate(float angleTo, float dt) {

        int dir = 1;

        if (angleTo > 90 && angleTo < 270) {
            dir = 1; //counterclockwse
        } else {
            dir = -1; //clockwise
        }

        mCurrentAngle +=  dir * mRotationSpeed * dt;
        if (mCurrentAngle > 360)
            mCurrentAngle = mCurrentAngle - 360;

        if (mCurrentAngle < 0)
            mCurrentAngle = mCurrentAngle + 360;

    }

    public void moveTo(Vector2 direction, float dt) {
        getPosition().x += mVelocity.x * direction.x * dt;
        getPosition().y += mVelocity.y * direction.y * dt;
    }

    public boolean handleInput(float dt) {
//        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
//            mShift.y += mVelocity.y;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
//            mShift.y -= mVelocity.y;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
//            mShift.x -= mVelocity.x;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
//            mShift.x += mVelocity.x;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.RIGHT) ) {
//            mCurrentAngle -= mRotationSpeed * dt;
//        }
//
//        if (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
//            mCurrentAngle += mRotationSpeed * dt;
//        }
//
//        return false;
        return false;
    }

    public abstract float getOriginX();
    public abstract float getOriginY();


    @Override
    public void draw() {
        if (mIsVisible) {
            mSpriteBatch.draw(
                    mTextureRegion,
                    getPosition().x,
                    getPosition().y,
                    getOriginX(),
                    getOriginY(),
                    mTextureRegion.getRegionWidth(),
                    mTextureRegion.getRegionHeight(),
                    1,
                    1,
                    mCurrentAngle
            );
            drawExplosions();
        }
    }

    private void drawExplosions() {
        Iterator<Explosion> iter = mExplosions.iterator();
        while (iter.hasNext())
            iter.next().draw();
    }

    @Override
    public int getDamage() {
        return mDamage;
    }

    public float getCurrentAngle() {
        return mCurrentAngle;
    }

    public boolean isFlickering() {
        return mFlickeringManager.isFlickering;
    }

    @Override
    public int getHealth() {
        return mHealth;
    }

    @Override
    public float getWeight() {
        return 0;
    }

    @Override
    public void setHealth(int health) {
        mHealth = health;
    }

    @Override
    public void setDamage(int damage) {
        mDamage = damage;
    }



    @Override
    public void setDamageMultiplier(int damageMultiplier) {
        mDamageMultiplier = damageMultiplier;
    }

    // TODO сделать абтрактный класс, который накладывает временные эффекты (щит, мерцание, увеличение урона)
    private static class FlickeringManager {
        float forHowLong;
        float flickerInterval;
        float timeOfFlickering;

        boolean isFlickering = false;

        /**
         * Заставляет объект мерцать.
         * @param timesToFlicker сколько раз должен моргнуть объект
         * @param flickeringTime в течении какого времени (в секундах) объект мерцает
         */
        void startFlicker(float timesToFlicker, float flickeringTime) {
            forHowLong = flickeringTime;
            timeOfFlickering = 0;
            flickerInterval = flickeringTime / (2 * timesToFlicker);
            isFlickering = true;
        }

        boolean isVisible(float ts) {
            if (!isFlickering)
                return true;

            timeOfFlickering += ts;

            if (timeOfFlickering > forHowLong) {
                isFlickering = false;
                return true;
            }


            if ((int)(timeOfFlickering / flickerInterval) % 2 == 0) {
                return false;
            }

            return true;
        }
    }

    @Override
    public boolean collide(CollidableGameObject otherObject) {
        Polygon p = new Polygon();
        if (otherObject instanceof Asteroid) {
            if (Intersector.intersectPolygons(this.getPolygon(), otherObject.getPolygon(), p)) {
                if (!isInvincible()) {
                    takeDamage(otherObject.getDamage());
                    setInvincible(3);
                    mCollideSound.play();
                }
            }
            return true;
        }

        if (otherObject instanceof Bullet) {
            if (((Bullet)otherObject).isPlayers())
                return true;

            if (Intersector.intersectPolygons(otherObject.getPolygon(), getPolygon(), p)) {
                if (isInvincible())
                    return false;
                takeDamage(otherObject.getDamage());
                otherObject.takeDamage(getDamage());
                Explosion e = mExplosionObjectPool.get();
                e.setPosition(otherObject.getPosition().x, otherObject.getPosition().y);
                mExplosions.add(e);

            }
            return true;
        }

        if (otherObject instanceof AbstractShip) {
            if (Intersector.intersectPolygons(getPolygon(), otherObject.getPolygon(), p)) {
                if (!isInvincible()) {
                    takeDamage(otherObject.getDamage());
                    setInvincible(3);
                    mCollideSound.play();
                }
            }
            return true;
        }

        if (otherObject instanceof PowerUp) {
            if (Intersector.intersectPolygons(getPolygon(), otherObject.getPolygon(), p)) {
                ((PowerUp) otherObject).apply(this);
            }
            return true;
        }

        return false;
    }
}
