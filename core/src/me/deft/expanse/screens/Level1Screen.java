package me.deft.expanse.screens;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.ExpanseGame2;
import me.deft.expanse.Global;
import me.deft.expanse.anim.Explosion;
import me.deft.expanse.objects.EventProducer;
import me.deft.expanse.objects.ObservableGameObject;
import me.deft.expanse.objects.enemies.AbstractEnemyShip;
import me.deft.expanse.objects.enemies.Asteroid;
import me.deft.expanse.objects.ship.Bullet;
import me.deft.expanse.objects.ship.Ship;
import me.deft.expanse.objects.utils.PeriodicEventsManager;
import me.deft.expanse.objects.utils.Squad;

/**
 * Created by iyakovlev on 17.07.17.
 */

public class Level1Screen extends BasicGameScreen {

    private PeriodicEventsManager mPowerupsManager;
    private PeriodicEventsManager mAsteroidManager;
    private PeriodicEventsManager mEnemySquadsManager;
    private ObservableGameObject.Observer mAsteroidObserver;
    private Squad.OnShipReadyListener mOnShipReadyListener;
    private Ship.FireListener mFireListener;
    private Squad mCurrentEnemySquad;

    public Level1Screen(SpriteBatch spriteBatch) {
        super(spriteBatch);
        mLives = 3;

        mAsteroidObserver = new ObservableGameObject.Observer() {
            @Override
            public void onDestroy() {
                addScore(10);
            }
        };
        mFireListener = new Ship.FireListener() {
            @Override
            public void onFire(Bullet b) {
                addObject(b);
            }
        };

        mOnShipReadyListener = new Squad.OnShipReadyListener() {
            @Override
            public void onShipReady(AbstractEnemyShip ship) {
                ship.setFireListener(mFireListener);
                addObject(ship);
            }
        };

    }

    @Override
    public void show() {
        super.show();
        mPowerupsManager = new PeriodicEventsManager(0.1f, 5); // раз в 5 сек с вероятность 10% выдаст поверап
        mPowerupsManager.addEventListener(new EventProducer.EventListener() {
            @Override
            public void onEventOccured() {
                addObject(mGameObjectsFactory.getRandomPowerUp());
            }
        });

        mAsteroidManager = new PeriodicEventsManager(0.5f, 2);
        mAsteroidManager.addEventListener(new EventProducer.EventListener() {
            @Override
            public void onEventOccured() {
                Asteroid a = mGameObjectsFactory.getRandomAsteroid(new Vector2(ExpanseGame2.DEFAULT_WIDTH - 1, (float) Math.random() * ExpanseGame2.DEFAULT_HEIGHT));
                a.setObserver(mAsteroidObserver); // т.к. пока следим только за очками, обойдемся одним обсервером
                addObject(a);
            }
        });

        mEnemySquadsManager = new PeriodicEventsManager(1f, 15);
        mEnemySquadsManager.addEventListener(new EventProducer.EventListener() {
            @Override
            public void onEventOccured() {
                mCurrentEnemySquad = mGameObjectsFactory.getRandomSquad();
                mCurrentEnemySquad.setOnShipReadyListener(mOnShipReadyListener);
                mCurrentEnemySquad.setTarget(mShip);
            }
        });
    }

    @Override
    public void renderAfter(float delta) {

    }

    @Override
    public void renderBefore(float delta) {

    }

    @Override
    public void update(float timeDelta) {
        super.update(timeDelta);

        mPowerupsManager.checkEvent(timeDelta);
        mAsteroidManager.checkEvent(timeDelta);
        mEnemySquadsManager.checkEvent(timeDelta);

        if (mCurrentEnemySquad != null && !mCurrentEnemySquad.isDone()) {
            mCurrentEnemySquad.checkEvent(timeDelta);
        }
    }
}
