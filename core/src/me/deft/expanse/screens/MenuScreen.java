package me.deft.expanse.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import me.deft.expanse.ExpanseGame2;
import me.deft.expanse.Global;
import me.deft.expanse.objects.Background;

/**
 * Created by iyakovlev on 17.07.17.
 */

public class MenuScreen implements Screen {

    SpriteBatch mSpriteBatch;
    Background mBackground;
    Button mButtonStart;
    Button mButtonExit;

    TextureRegion mLogo;

    Music mTheme;

    final int BTN_LEFT_ALIGNMENT = ExpanseGame2.DEFAULT_WIDTH / 2 - 85;


    public MenuScreen(SpriteBatch spriteBatch) {
        mSpriteBatch = spriteBatch;
        mBackground = new Background(mSpriteBatch);
        mButtonStart = new Button(
                mSpriteBatch,
                Global.getInstance().getCurrentTextureAtlas().findRegion("btn_start"));

        mButtonExit = new Button(
                mSpriteBatch,
                Global.getInstance().getCurrentTextureAtlas().findRegion("btn_exit"));



        mButtonStart.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick() {
                ScreensManager.getInstance().setScreen(ScreenType.LEVEL_1);
            }
        });

        mButtonExit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick() {
                Gdx.app.exit();
            }
        });

        mLogo = Global.getInstance().getCurrentTextureAtlas().findRegion("logo");

        mTheme = Global.getInstance().getAssetManager().get("theme.mp3", Music.class);
        mTheme.setLooping(true);

    }

    @Override
    public void show() {
        mTheme.play();
        mButtonStart.setPosition(BTN_LEFT_ALIGNMENT, ExpanseGame2.DEFAULT_HEIGHT / 2);
        mButtonExit.setPosition(BTN_LEFT_ALIGNMENT, ExpanseGame2.DEFAULT_HEIGHT / 2 - 50 - 10);

    }

    @Override
    public void render(float delta) {
        update(delta);
        mSpriteBatch.begin();
        mSpriteBatch.draw(
                mLogo,
                ExpanseGame2.DEFAULT_WIDTH / 2 - mLogo.getRegionWidth() / 2,
                ExpanseGame2.DEFAULT_HEIGHT - mLogo.getRegionHeight() - 50);
        mBackground.draw();
        mButtonStart.draw();
        mButtonExit.draw();
        mSpriteBatch.end();
    }

    private void update(float delta) {
        mBackground.update(delta);
        mButtonStart.update(delta);
        mButtonExit.update(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        mTheme.pause();
    }

    @Override
    public void resume() {
        mTheme.play();
    }

    @Override
    public void hide() {
        mTheme.pause();
    }

    @Override
    public void dispose() {
        mTheme.dispose();
    }


}
