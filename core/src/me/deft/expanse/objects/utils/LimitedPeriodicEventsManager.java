package me.deft.expanse.objects.utils;

/**
 * Created by iyakovlev on 21.07.17.
 */

public class LimitedPeriodicEventsManager extends PeriodicEventsManager {

    protected int mNumberOfEvents;

    public LimitedPeriodicEventsManager(float probability, float seconds, int numberOfEvents) {
        super(probability, seconds);
        mNumberOfEvents = numberOfEvents;
    }

    @Override
    public boolean checkEvent(float dt) {
        if (mNumberOfEvents == 0)
            return false;
        if (super.checkEvent(dt)) {
            mNumberOfEvents--;
            return true;
        }
        return false;
    }
}
