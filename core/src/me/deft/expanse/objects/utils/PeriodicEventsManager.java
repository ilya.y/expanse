package me.deft.expanse.objects.utils;

import me.deft.expanse.objects.EventProducer;

/**
 * Created by iyakovlev on 14.07.17.
 */

public class PeriodicEventsManager implements EventProducer{

    private final float mProbability;
    private final float mAttemptsInterval;
    private float mCurrentTimeElapsed;
    private EventListener mListener;

    public PeriodicEventsManager(float probability, float seconds) {
        mProbability = probability;
        mAttemptsInterval = seconds;
        mCurrentTimeElapsed = 0f;
    }

    public void addEventListener(EventListener listener) {
        mListener = listener;
    }

    @Override
    public boolean checkEvent(float dt) {
        mCurrentTimeElapsed += dt;

        if (mCurrentTimeElapsed > mAttemptsInterval) {
            mCurrentTimeElapsed = 0;
            double dice = Math.random();
            if (dice < mProbability) {
                if (mListener != null)
                    mListener.onEventOccured();
                return true;
            }
            return false;
        }
        return false;
    }
}
