package me.deft.expanse.objects.utils;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;

import me.deft.expanse.ExpanseGame2;

/**
 * Created by iyakovlev on 15.07.17.
 */

public class GameInputProcessor implements InputProcessor {

    public class TouchInfo {
        Vector2 touch = new Vector2();
        boolean mIsTouched;
        boolean mIsDragged;
        Vector2 drag = new Vector2();
    }

    public HashMap<Integer, TouchInfo> mTouchInfoHashMap = new HashMap<>();

    public GameInputProcessor() {
        for (int i = 0; i < 6; i++) {
            mTouchInfoHashMap.put(i, new TouchInfo());
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        mTouchInfoHashMap.get(pointer).touch.x = screenX;
        mTouchInfoHashMap.get(pointer).touch.y = screenY;
        mTouchInfoHashMap.get(pointer).mIsTouched = true;
//        System.out.println( mTouchInfoHashMap.get(pointer).touch);
        mTouchInfoHashMap.get(pointer).touch = CoordinatesManager.unproject(mTouchInfoHashMap.get(pointer).touch);
//        System.out.println( mTouchInfoHashMap.get(pointer).touch);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        mTouchInfoHashMap.get(pointer).touch.x = 0;
        mTouchInfoHashMap.get(pointer).touch.y = 0;
        mTouchInfoHashMap.get(pointer).mIsTouched = false;
        mTouchInfoHashMap.get(pointer).drag.x = 0;
        mTouchInfoHashMap.get(pointer).drag.y = 0;
        mTouchInfoHashMap.get(pointer).mIsDragged = false;
        return true;
    }

    public boolean checkTouchRegion(int x, int y, int width, int height) {
        for (TouchInfo touchInfo : mTouchInfoHashMap.values()) {
            if (touchInfo.touch.x > x && touchInfo.touch.x < x + width && touchInfo.touch.y > y && touchInfo.touch.y < y + height) {
                return touchInfo.mIsTouched;
            }
        }
        return false;
    }

    public boolean checkDraggedFromRegion(int x, int y, int width, int height) {
        for (TouchInfo touchInfo : mTouchInfoHashMap.values()) {
            if (touchInfo.touch.x > x && touchInfo.touch.x < x + width && touchInfo.touch.y > y && touchInfo.touch.y < y + height) {
                //System.out.println("isTouched " + touchInfo.mIsTouched + " is dragged" + touchInfo.mIsDragged);
                return touchInfo.mIsTouched && touchInfo.mIsDragged;
            }
        }
        return false;
    }

    public Vector2 getDragDirectionForRegion(int x, int y, int width, int height) {
        for (TouchInfo touchInfo : mTouchInfoHashMap.values()) {
            if (touchInfo.touch.x > x && touchInfo.touch.x < x + width && touchInfo.touch.y > y && touchInfo.touch.y < y + height) {
                Vector2 direction = new Vector2();
                direction.x = touchInfo.drag.x - touchInfo.touch.x;
                direction.y = touchInfo.drag.y - touchInfo.touch.y;
                return direction.nor();
            }
        }
        return null;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
       // System.out.println("drag " + pointer);
        mTouchInfoHashMap.get(pointer).mIsDragged = true;
        mTouchInfoHashMap.get(pointer).drag.x = screenX;
        mTouchInfoHashMap.get(pointer).drag.y = screenY;
        mTouchInfoHashMap.get(pointer).drag = CoordinatesManager.unproject(mTouchInfoHashMap.get(pointer).drag);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
