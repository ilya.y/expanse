package me.deft.expanse.objects;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by iyakovlev on 28.06.17.
 */
public interface GameObject {

    void draw();

    void update(float timeDelta);

    float getWidth();

    float getHeight();

    Vector2 getPosition();

    void setPosition(float x, float y);


}
