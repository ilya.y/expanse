package me.deft.expanse.objects.ship;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.objects.AbstractCollidableGameObject;
import me.deft.expanse.objects.AbstractGameObject;
import me.deft.expanse.objects.CollidableGameObject;
import me.deft.expanse.objects.utils.ObjectPool;

/**
 * Created by iyakovlev on 06.07.17.
 */

public class Bullet extends AbstractCollidableGameObject
        implements CollidableGameObject, ObjectPool.PoolItem<Bullet> {

    private ObjectPool<Bullet> mBulletObjectPool;
    private boolean mIsPlayers = false;

    private final Polygon mPolygon = new Polygon(new float[] {
            0, 0,
            0, 2,
            2, 4,
            4, 0,
    });

    public Bullet(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture);
        mDamage = 0;
    }

    public void init(Vector2 velocity, Vector2 position, int damage) {
        mVelocity = velocity;
        mHealth = 1;
        setPosition(position);
        mPolygon.setPosition(position.x, position.y);
        mDamage = damage;
    }

    @Override
    protected void onZeroHealth() {

    }

    public void setIsPlayers(boolean players) {
        mIsPlayers = players;
    }

    public boolean isPlayers() {
        return mIsPlayers;
    }

    @Override
    public boolean isNeedToBeDestroyed() {
        return mHealth <= 0;
    }

    @Override
    public void draw() {
        mSpriteBatch.draw(mTextureRegion, getPosition().x, getPosition().y);
    }

    @Override
    public void update(float timeDelta) {
        super.update(timeDelta);
        setPosition(getPosition().add(getVelocity().cpy().scl(timeDelta)));
        mPolygon.setPosition(getPosition().x, getPosition().y);
    }

    @Override
    public Polygon getPolygon() {
       return mPolygon;
    }

    @Override
    public Vector2 getVelocity() {
        return mVelocity;
    }

    @Override
    public int getDamage() {
        return mDamage;
    }

    @Override
    public void takeDamage(int dmg) {
        mHealth -= dmg;
    }

    @Override
    public float getWeight() {
        return 0.01f;
    }

    @Override
    public void dispose() {
        mBulletObjectPool.recycle(this);
    }

    @Override
    public void setPool(ObjectPool<Bullet> pool) {
        mBulletObjectPool = pool;
    }

    @Override
    public int getHealth() {
        return mHealth;
    }

    @Override
    public boolean collide(CollidableGameObject otherObject) {
        return false;
    }
}
