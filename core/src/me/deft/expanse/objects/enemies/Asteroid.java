package me.deft.expanse.objects.enemies;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.Polygon;

import java.util.LinkedList;
import java.util.List;

import me.deft.expanse.Global;
import me.deft.expanse.objects.AbstractCollidableGameObject;
import me.deft.expanse.objects.CollidableGameObject;
import me.deft.expanse.objects.ObservableGameObject;
import me.deft.expanse.objects.ship.Bullet;
import me.deft.expanse.objects.ship.Ship;
import me.deft.expanse.objects.utils.ObjectPool;

/**
 * Created by iyakovlev on 29.06.17.
 */
public class Asteroid extends AbstractCollidableGameObject
        implements ObjectPool.PoolItem<Asteroid>, CollidableGameObject, ObservableGameObject {

    private int mTextureWidth;
    private int mTextureHeight;

    private float mWeight;

    private ObjectPool<Asteroid> mAsteroidObjectPool;
    private Sound mSound;

    private Observer mObserver;

    private final com.badlogic.gdx.math.Polygon mPolygon = new Polygon(new float[] {
            0, 120,
            32, 200,
            80, 235,
            203, 229,
            255, 140,
            176, 0,
            99, 20,
            29, 40,
    });



    public Asteroid(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture);
        mVelocity = new Vector2(0,0);
        mTextureHeight = texture.getRegionHeight();
        mTextureWidth = texture.getRegionWidth();
        mSound = Global.getInstance().getAssetManager().get("rock.mp3", Sound.class);
    }

    public void init(Vector2 speed, float x, float y, float weight, int health) {
        mVelocity = speed;
        mWeight = weight;
        mHealth = (int) (mWeight * health);
        mPolygon.setScale(mWeight, mWeight);
        mPolygon.setPosition(x, y);
        setPosition(x, y);
        mObserver = null;
    }

    public void setSpeed(Vector2 speed) {
        mVelocity = speed;
    }

    @Override
    public void setObserver(Observer observer) {
        mObserver = observer;
    }

    @Override
    public void removeObserver() {
        mObserver = null;
    }

    @Override
    public void update(float timeDelta) {
        float newX = mVelocity.x * timeDelta;
        float newY = mVelocity.y * timeDelta;
        setPosition(getPosition().x + newX, getPosition().y + newY);
        mPolygon.setPosition(getPosition().x, getPosition().y);
    }

    @Override
    public int getHealth() {
        return mHealth;
    }

    @Override
    public Polygon getPolygon() {
        return mPolygon;
//        Polygon p = new Polygon(mPolygon.getVertices());
//        p.setPosition(getPosition().x, getPosition().y);
//        p.setScale(mWeight, mWeight);
//        return p;
    }

    @Override
    public boolean isNeedToBeDestroyed() {
       return mHealth <= 0;
    }

    @Override
    public int getDamage() {
        return 3;
    }

    @Override
    public void dispose() {

        mAsteroidObjectPool.recycle(this);
    }

    @Override
    public void setPool(me.deft.expanse.objects.utils.ObjectPool<Asteroid> pool) {
        mAsteroidObjectPool = pool;
    }

    @Override
    public Vector2 getVelocity() {
        return mVelocity;
    }

    @Override
    public void draw() {
        if (mIsVisible) {
            mSpriteBatch.draw(
                    mTextureRegion,
                    getPosition().x,
                    getPosition().y,
                    0,
                    0,
                    mTextureWidth,
                    mTextureHeight,
                    mWeight,
                    mWeight,
                    0
                    );
        }
    }

    @Override
    protected void onZeroHealth() {
        mSound.play();
        if (mObserver != null)
            mObserver.onDestroy();
    }

    @Override
    public float getWeight() {
        return mWeight;
    }

    @Override
    public float getWidth() {
        return mTextureWidth * mWeight;
    }

    @Override
    public float getHeight() {
        return mTextureHeight * mWeight;
    }

    @Override
    public boolean collide(CollidableGameObject otherObject) {
        if (otherObject instanceof Asteroid) {
            Intersector.MinimumTranslationVector mtv = new Intersector.MinimumTranslationVector();

            if (Intersector.overlapConvexPolygons(this.getPolygon(), otherObject.getPolygon(), mtv)) {

                if (Intersector.overlapConvexPolygons(this.getPolygon(), otherObject.getPolygon(), mtv)) {

                    Vector2 cs = this.getVelocity();
                    Vector2 os = otherObject.getVelocity();

                    float mc = this.getWeight();
                    float mo = otherObject.getWeight();


                    Vector2 newCurAstSpeed = new Vector2();
                    Vector2 newOthAstSpeed = new Vector2();

                    newCurAstSpeed.x = (2 * mo * os.x + (mc - mo) * cs.x) / (mc + mo);
                    newCurAstSpeed.y = (2 * mo * os.y + (mc - mo) * cs.y) / (mc + mo);

                    newOthAstSpeed.x = (2 * mc * cs.x + (mo - mc) * os.x) / (mc + mo);
                    newOthAstSpeed.y = (2 * mc * cs.y + (mo - mc) * os.y) / (mc + mo);

                    this.setSpeed(newCurAstSpeed);
                    ((Asteroid) otherObject).setSpeed(newOthAstSpeed);


                    //делаем перенос полигонов, что бы они не наезжали друг в друга, иначе могут склеиться
                    this.getPosition().mulAdd(mtv.normal, mtv.depth);

                    otherObject.getPosition().mulAdd(mtv.normal, -mtv.depth);

                }
            }
            return true;
        }

        Polygon p = new Polygon();
        if (otherObject instanceof Bullet) {
            Bullet bullet = (Bullet) otherObject;
            if (Intersector.intersectPolygons(bullet.getPolygon(), this.getPolygon(), p)) {
                this.takeDamage(bullet.getDamage());
                bullet.takeDamage(this.getDamage());

                Vector2 cs = bullet.getVelocity();
                Vector2 os = this.getVelocity();

                float mc = bullet.getWeight();
                float mo = this.getWeight();


                Vector2 newCurAstSpeed = new Vector2();
                Vector2 newOthAstSpeed = new Vector2();

                newCurAstSpeed.x = (2 * mo * os.x + (mc - mo) * cs.x) / (mc + mo);
                newCurAstSpeed.y = (2 * mo * os.y + (mc - mo) * cs.y) / (mc + mo);

                newOthAstSpeed.x = (2 * mc * cs.x + (mo - mc) * os.x) / (mc + mo);
                newOthAstSpeed.y = (2 * mc * cs.y + (mo - mc) * os.y) / (mc + mo);

                this.setSpeed(newOthAstSpeed);
            }
            return true;
        }

        return false;
    }
}
