package me.deft.expanse.objects.ship;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.Global;
import me.deft.expanse.objects.utils.AbstractFactory;
import me.deft.expanse.objects.utils.ObjectPool;

/**
 * Created by iyakovlev on 06.07.17.
 */

public class MachineGun implements Gun {

    SpriteBatch mSpriteBatch;
    ObjectPool<Bullet> mBulletObjectPool;

    private float mFireRate = 0.1f; // 10 выстрелов в секунду
    private float mCurrentCooldown;
    private float mAngle;
    private Vector2 mPosition;
    private Vector2 mBulletVelocity = new Vector2(300, 0);
    private int mDamage;

    public MachineGun(SpriteBatch spriteBatch) {
        mSpriteBatch = spriteBatch;
        mBulletObjectPool = new ObjectPool<>(new BulletFactory(spriteBatch, Global.getInstance().getCurrentTextureAtlas()));
        mPosition = new Vector2(0,0);
        mDamage = 1;
    }

    public void setDamage(int damage) {
        mDamage = damage;
    }

    @Override
    public Bullet shoot(float dt) {
       if (mCurrentCooldown == 0) {
           Bullet b =  mBulletObjectPool.get();
           b.init(mBulletVelocity.cpy().rotate(mAngle), mPosition.cpy(), mDamage);
           mCurrentCooldown += dt;
           return b;
       } else {
           mCurrentCooldown += dt;
           if (mCurrentCooldown >= mFireRate) {
               mCurrentCooldown = 0;
           }

           return null;
       }

    }

    @Override
    public void setPosition(Vector2 position) {
        mPosition.x = position.x;
        mPosition.y = position.y;
    }

    @Override
    public void setFireRate(float seconds) {
        mFireRate = seconds;
    }

    @Override
    public void setAngle(float angle) {
        mAngle = angle;
    }

    @Override
    public float getAngle() {
        return mAngle;
    }

    private class BulletFactory extends AbstractFactory<Bullet> {

        private TextureRegion mBulletTexture;
        public BulletFactory(SpriteBatch spriteBatch, TextureAtlas textureAtlas) {
            super(spriteBatch, textureAtlas);
            mBulletTexture = textureAtlas.findRegion("bullet");
        }

        @Override
        public Bullet get() {
            return new Bullet(mSpriteBatch,  mBulletTexture);
        }
    }

}
