package me.deft.expanse.objects.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.Random;

import me.deft.expanse.objects.enemies.Asteroid;
import me.deft.expanse.objects.utils.AbstractFactory;

/**
 * Created by iyakovlev on 05.07.17.
 */

public class AsteroidFactory extends AbstractFactory<Asteroid> {

    private TextureRegion mTextureRegion;

    public AsteroidFactory(SpriteBatch spriteBatch, TextureAtlas textureAtlas) {
        super(spriteBatch, textureAtlas);
        if (mTextureRegion == null)
            mTextureRegion = textureAtlas.findRegion("asteroid");
    }

    @Override
    public Asteroid get() {
        return new Asteroid(mSpriteBatch, mTextureRegion);
    }
}
