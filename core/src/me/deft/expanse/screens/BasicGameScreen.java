package me.deft.expanse.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

import me.deft.expanse.ExpanseGame2;
import me.deft.expanse.Global;
import me.deft.expanse.objects.Background;
import me.deft.expanse.objects.CollidableGameObject;
import me.deft.expanse.objects.EventProducer;
import me.deft.expanse.objects.GameObject;
import me.deft.expanse.objects.ship.Bullet;
import me.deft.expanse.objects.ship.Ship;
import me.deft.expanse.objects.utils.GameInputProcessor;
import me.deft.expanse.objects.utils.GameObjectsFactory;
import me.deft.expanse.objects.utils.ObjectPool;
import me.deft.expanse.objects.utils.Stick;

/**
 * Created by iyakovlev on 17.07.17.
 */

public abstract class BasicGameScreen implements Screen {

    private static final int SHIP_PADDING = 20; // количество пикселей от кромки экрана

    private boolean mIsDebug;
    protected SpriteBatch mSpriteBatch;
    protected static GameObjectsFactory mGameObjectsFactory;
    protected BitmapFont mBitmapFont;

    protected int mScore;

    private me.deft.expanse.screens.Button mRestartButton;
    ShapeRenderer mDebugRenderer;

    Array<CollidableGameObject> mActiveObjects = new Array<>();

    Background mBackground;

    protected Ship mShip;

    protected Stick mL;
    protected Stick mR;

    protected int mLives;

    private GameInputProcessor mGameInputProcessor;

    public BasicGameScreen(SpriteBatch spriteBatch) {
        mSpriteBatch = spriteBatch;
        mGameInputProcessor = Global.getInstance().getGameInputProcessor();
        Gdx.input.setInputProcessor(mGameInputProcessor);
        if (mGameObjectsFactory == null)
            mGameObjectsFactory = new GameObjectsFactory(spriteBatch);

        if (mBitmapFont == null)
            mBitmapFont = Global.getInstance().getFont();

        if (ExpanseGame2.IS_ANDROID) {
            mL = new Stick(mSpriteBatch);
            final int MARGIN = 50;
            mL.setPosition(MARGIN, MARGIN);
            mR = new Stick(mSpriteBatch);
            mR.setPosition(ExpanseGame2.DEFAULT_WIDTH - MARGIN - mR.getWidth(), MARGIN);
        }

        mDebugRenderer = new ShapeRenderer();
        mDebugRenderer.setAutoShapeType(true);
        mDebugRenderer.setColor(Color.RED);
        mBackground = new Background(mSpriteBatch);

        mLives = 3;

        mScore = 0;

        mRestartButton = new me.deft.expanse.screens.Button(
                mSpriteBatch,
                Global.getInstance().getCurrentTextureAtlas().findRegion("btn_start"));

        mRestartButton.setOnClickListener(new me.deft.expanse.screens.Button.OnClickListener() {
            @Override
            public void onClick() {
                ScreensManager.getInstance().setScreen(ScreenType.LEVEL_1);
            }
        });

        mRestartButton.setPosition(
                ExpanseGame2.DEFAULT_WIDTH / 2 - mRestartButton.getWidth() / 2,
                ExpanseGame2.DEFAULT_HEIGHT / 2 - 120
        );


    }

    public int getScore() {
        return mScore;
    }

    public void addScore(int add) {
        mScore += add;
    }


    protected void addObject(CollidableGameObject object) {
        if (object != null)
            mActiveObjects.add(object);
    }

    private void drawDebug() {
        if (ExpanseGame2.IS_DEBUG) {
            mDebugRenderer.begin();
            for (CollidableGameObject o : mActiveObjects) {
                mDebugRenderer.polygon(o.getPolygon().getTransformedVertices());
            }
            mDebugRenderer.polygon(mShip.getPolygon().getTransformedVertices());
            mDebugRenderer.end();
        }
    }

    @Override
    public void show() {
        mShip = mGameObjectsFactory.getShip();
        mShip.addOnFireListener(new Ship.FireListener() {
            @Override
            public void onFire(Bullet b) {
                addObject(b);
            }
        });
        mShip.addObserver(new Ship.ShipObserver() {
            @Override
            public void onHealthChanged(int health) {
                if (health <= 0) {
                    mLives--;
                    mShip.setHealth(mShip.getMaximumHealth());
                }
            }
        });
    }

    public abstract void renderBefore(float delta);

    public abstract void renderAfter(float delta);

    protected boolean isOverBounds(GameObject object) {
        boolean isOverBounds = object.getPosition().x + object.getWidth() < 0
                || object.getPosition().y + object.getHeight() < 0
                || object.getPosition().y > ExpanseGame2.DEFAULT_HEIGHT;

        return isOverBounds;
    }

    private void checkShipBounds() {
        // право
        if (mShip.getPosition().x + mShip.getWidth() + SHIP_PADDING > ExpanseGame2.DEFAULT_WIDTH) {
            mShip.setPosition(ExpanseGame2.DEFAULT_WIDTH - mShip.getWidth() - SHIP_PADDING, mShip.getPosition().y);
        }

        // верх
        if (mShip.getPosition().y + mShip.getHeight() + SHIP_PADDING > ExpanseGame2.DEFAULT_HEIGHT) {
            mShip.setPosition(mShip.getPosition().x, ExpanseGame2.DEFAULT_HEIGHT - mShip.getHeight() - SHIP_PADDING);
        }

        // лево
        if (mShip.getPosition().x - SHIP_PADDING < 0) {
            mShip.setPosition(SHIP_PADDING, mShip.getPosition().y);
        }

        // низ
        if (mShip.getPosition().y - SHIP_PADDING < 0) {
            mShip.setPosition(mShip.getPosition().x, SHIP_PADDING);
        }
    }

    @Override
    public void render(float delta) {
        update(delta);
        mSpriteBatch.begin();
        if (mLives == 0) {
            drawGameOver();
            mSpriteBatch.end();
            return;
        }

        mBackground.draw();

        renderBefore(delta);
        renderObjects();
        mShip.draw();
        renderAfter(delta);
        renderGUI();
        mSpriteBatch.end();

        drawDebug();
    }

    void drawGameOver() {
        mBitmapFont.draw(mSpriteBatch, "GAME OVER", ExpanseGame2.DEFAULT_WIDTH / 2 - 40, ExpanseGame2.DEFAULT_HEIGHT / 2);
        mRestartButton.draw();
    }

    public void update(float delta) {
        if (mLives == 0) {
            mRestartButton.update(delta);
            return;
        }
        processControls(delta);
        cleanActiveObjects();
        updateObjects(delta);
        processCollisions();
        checkShipBounds();
        mBackground.update(delta);
        mShip.update(delta);
    }

    private void cleanActiveObjects() {
        Iterator<CollidableGameObject> iterator = mActiveObjects.iterator();
        while (iterator.hasNext()) {
            CollidableGameObject o = iterator.next();

            if (o.isNeedToBeDestroyed() || isOverBounds(o)) {
                iterator.remove();
                if (o instanceof ObjectPool.PoolItem) {
                    // не очень красиво, но не хотелось городить ради этого отдельный интерфейс
                    ((ObjectPool.PoolItem)o).dispose();
                }
                continue;
            }
        }
    }


    private void updateObjects(float dt) {
        Iterator<CollidableGameObject> iterator = mActiveObjects.iterator();
        while (iterator.hasNext()) {
            iterator.next().update(dt);
        }
    }

    private void renderObjects() {
        Iterator<CollidableGameObject> iterator = mActiveObjects.iterator();
        while (iterator.hasNext()) {
            iterator.next().draw();
        }
    }

    private void processCollisions() {
        for (int i = mActiveObjects.size - 1; i > 0; i--) {
            for (int j = i - 1; j >= 0; j--) {
                if (!mActiveObjects.get(i).collide(mActiveObjects.get(j)))
                    mActiveObjects.get(j).collide(mActiveObjects.get(i));
            }
            mShip.collide(mActiveObjects.get(i));
        }
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void renderGUI() {
        mBitmapFont.draw(mSpriteBatch, "HP: " + mShip.getHealth(), 20, ExpanseGame2.DEFAULT_HEIGHT - 40);
        mBitmapFont.draw(mSpriteBatch, "Lives: " + mLives, 20, ExpanseGame2.DEFAULT_HEIGHT - 65);
        mBitmapFont.draw(mSpriteBatch, "Score: " + mScore, 20, ExpanseGame2.DEFAULT_HEIGHT - 90);

        if (ExpanseGame2.IS_ANDROID) {
            mL.draw();
            mR.draw();
        }
    }

    private void processControls(float dt) {
        if (ExpanseGame2.IS_ANDROID) {
            mL.update(dt);
            if (mL.isActive())
                mShip.moveTo(mL.getDirectionNor(), dt);

            mR.update(dt);
            if (mR.isActive())
                mShip.rotate(mR.getDirectionNor().angle(), dt);

        } else {
            int x = 0, y = 0;
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                y = 1;
            }

            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                y = -1;
            }

            if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                x = -1;
            }

            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                x = 1;
            }
            mShip.moveTo(new Vector2(x, y), dt);

            if (Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                mShip.rotate(0, dt);
            }

            if (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                mShip.rotate(180, dt);
            }
        }
    }
}
