package me.deft.expanse.objects.utils;

import com.badlogic.gdx.math.Vector2;

import java.util.Arrays;

/**
 * Created by iyakovlev on 09.07.17.
 */

public class Route implements ObjectPool.PoolItem<Route> {

    private ObjectPool<Route> mRouteObjectPool;

    float[] mVertexes;
    boolean mIsFinished = false;

    private Vector2 mPointTo;

    private int mNextPoint = 0;


    public void init(float[] vertexes) {
        if (vertexes.length % 2 != 0)
            throw new IllegalArgumentException("Route should have even number of vertexes");
        mVertexes = vertexes;
    }

    public Vector2 getNextPoint() {
        if (mNextPoint < mVertexes.length) {
            mPointTo = new Vector2(mVertexes[mNextPoint], mVertexes[mNextPoint + 1]);
            mNextPoint += 2;
        }
        if (mNextPoint > mVertexes.length - 1)
            mIsFinished = true;

        return mPointTo;
    }

    public boolean isFinished() {
        return mIsFinished;
    }

    @Override
    public void dispose() {
        mRouteObjectPool.recycle(this);
    }

    @Override
    public void setPool(ObjectPool<Route> pool) {
        mRouteObjectPool = pool;
    }

    public Route cpy() {
        Route cpy = new Route();
        cpy.setPool(mRouteObjectPool);
        cpy.mIsFinished = isFinished();
        cpy.mVertexes = Arrays.copyOf(mVertexes, mVertexes.length);
        cpy.mNextPoint = mNextPoint;
        cpy.mPointTo = mPointTo == null ? null : mPointTo.cpy();
        return cpy;
    }
}
