package me.deft.expanse.objects.ship;

import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.objects.CollidableGameObject;

/**
 * Created by iyakovlev on 03.07.17.
 */
public interface Ship extends CollidableGameObject {



    interface FireListener {
        void onFire(Bullet b);
    }

    interface ShipObserver {

        void onHealthChanged(int health);
    }


    void addObserver(ShipObserver observer);

    void removeObserver(ShipObserver observer);

    void setInvincible(float time);

    boolean isInvincible();

    void setHealth( int health);

    void setDamageMultiplier(int damageMultiplier);

    int getMaximumHealth();

    void shoot(float dt);

    void moveTo(Vector2 direction, float dt);

    void rotate(float angle, float dt);

    void addOnFireListener(FireListener listener);

}
