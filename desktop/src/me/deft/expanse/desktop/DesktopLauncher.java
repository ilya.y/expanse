package me.deft.expanse.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import me.deft.expanse.ExpanseGame2;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = ExpanseGame2.DEFAULT_WIDTH;
		config.height = ExpanseGame2.DEFAULT_HEIGHT;

		config.title = "Expanse";

		boolean isDebug = false;
		for (String s : arg)
			if (s.equals("--debug"))
				isDebug = true;

		ExpanseGame2.setDebug(isDebug);

		new LwjglApplication(new ExpanseGame2(), config);
	}
}
