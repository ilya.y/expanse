package me.deft.expanse.anim;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.objects.GameObject;
import me.deft.expanse.objects.utils.ObjectPool;

/**
 * Created by iyakovlev on 21.07.17.
 */

public class Explosion extends Animation<TextureRegion>
        implements GameObject, ObjectPool.PoolItem<Explosion>{

    SpriteBatch mSpriteBatch;

    float mWidth;
    float mHeight;

    float dt = 0;

    private ObjectPool<Explosion> mExplosionObjectPool;
    private Vector2 mPosition;


    public Explosion(SpriteBatch spriteBatch, float frameDuration, Texture sprite, int numberOfFrames) {
        super(frameDuration, TextureRegion.split(sprite, sprite.getWidth()  / numberOfFrames, sprite.getHeight())[0]);
        mSpriteBatch = spriteBatch;
        setPlayMode(PlayMode.NORMAL);
        mWidth = sprite.getWidth()  / numberOfFrames;
        mHeight = sprite.getHeight();
        mPosition = new Vector2();
    }

    public boolean isFinished() {
        return isAnimationFinished(dt);
    }

    @Override
    public void draw() {
        TextureRegion t = getKeyFrame(dt);
        mSpriteBatch.draw(t, getPosition().x, getPosition().y);
    }

    public void reset() {
        dt = 0;
    }

    @Override
    public void update(float timeDelta) {
        dt += timeDelta;
    }

    @Override
    public float getWidth() {
        return mWidth;
    }

    @Override
    public float getHeight() {
        return mHeight;
    }

    @Override
    public Vector2 getPosition() {
        return mPosition;
    }

    @Override
    public void setPosition(float x, float y) {
        mPosition.set(x, y);
    }

    @Override
    public void dispose() {
        mExplosionObjectPool.recycle(this);
        reset();
    }

    @Override
    public void setPool(ObjectPool<Explosion> pool) {
        mExplosionObjectPool = pool;
    }
}
