package me.deft.expanse.objects.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import me.deft.expanse.Global;
import me.deft.expanse.objects.GameObject;

/**
 * Created by iyakovlev on 15.07.17.
 */

public class Stick implements GameObject {

    Vector2 mPosition = new Vector2();
    boolean mIsActive;
    Vector2 mDirection;

    SpriteBatch mSpriteBatch;
    TextureRegion mTextureRegion;
    GameInputProcessor mGameInputProcessor;

    public Stick(SpriteBatch spriteBatch) {
        mSpriteBatch = spriteBatch;
        mTextureRegion = Global.getInstance().getCurrentTextureAtlas().findRegion("joystick");
        mGameInputProcessor = Global.getInstance().getGameInputProcessor();
        mPosition.x = (getPosition().x + mTextureRegion.getRegionWidth()) / 2;
        mPosition.y = (getPosition().y + mTextureRegion.getRegionHeight()) / 2;
        mDirection = new Vector2(0,0);
    }

    @Override
    public void draw() {
        mSpriteBatch.draw(mTextureRegion, mPosition.x, mPosition.y);
    }

    public boolean isActive() {
        return mIsActive;
    }

    public Vector2 getDirectionNor() {
        return mDirection;
    }

    @Override
    public void update(float timeDelta) {
        mIsActive = mGameInputProcessor.checkDraggedFromRegion(
                (int) getPosition().x,
                (int) getPosition().y,
                mTextureRegion.getRegionWidth(),
                mTextureRegion.getRegionHeight());

        if (mIsActive) {
            Vector2 vector = mGameInputProcessor.getDragDirectionForRegion(
                    (int) getPosition().x,
                    (int) getPosition().y,
                    mTextureRegion.getRegionWidth(),
                    mTextureRegion.getRegionHeight());
            if (vector != null) {
                mDirection.x = vector.x;
                mDirection.y = vector.y;
            } else {
                mDirection.x = 0;
                mDirection.y = 0;
            }
        } else {
            mDirection.x = 0;
            mDirection.y = 0;
        }
    }

    @Override
    public float getWidth() {
        return mTextureRegion.getRegionWidth();
    }

    @Override
    public float getHeight() {
        return mTextureRegion.getRegionHeight();
    }

    @Override
    public Vector2 getPosition() {
        return mPosition;
    }

    @Override
    public void setPosition(float x, float y) {
        mPosition.x = x;
        mPosition.y = y;
    }


}
