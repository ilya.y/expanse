package me.deft.expanse.objects.ship;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by iyakovlev on 03.07.17.
 */
public class Rocinante extends AbstractShip {

    private final static int MAX_HEALTH = 25;
    private static final com.badlogic.gdx.math.Polygon mPolygon = new Polygon(new float[] {
            4, 4,
            4, 60,
            140, 60,
            182, 30,
            140, 4
    });

    private Vector2 mGunPosition;
    private Gun mGun;


    public Rocinante(SpriteBatch spriteBatch, TextureRegion texture) {
        super(spriteBatch, texture, new Vector2(300, 300));
        mGun = new MachineGun(spriteBatch);
        mGun.setDamage(2);
        mGunPosition = new Vector2(0,0);
        setHealth(MAX_HEALTH);
    }

    @Override
    public Polygon getPolygon() {
        return mPolygon;
    }

    @Override
    public void update(float timeDelta) {
        super.update(timeDelta);
        mGunPosition.set(getPosition().x + getOriginX() - 10, getPosition().y + getOriginY());
        mGun.setPosition(mGunPosition);
        mGun.setAngle(getCurrentAngle());
        shoot(timeDelta);
    }

    public void shoot(float dt) {
        Bullet b = mGun.shoot(dt);
        if (b != null) {
            b.setDamage(b.getDamage() * mDamageMultiplier);
            b.setIsPlayers(true);
            mFireListener.onFire(b);
        }
    }

    private Vector2 getGunPosition() {
        return mGunPosition;
    }

    @Override
    public float getOriginX() {
        return 180;
    }

    @Override
    public float getOriginY() {
        return 30;
    }



    @Override
    public int getMaximumHealth() {
        return MAX_HEALTH;
    }
}
